FROM maven:3.5.2-jdk-8

COPY . / 
RUN mvn install -B


CMD ["mvn", "run"]